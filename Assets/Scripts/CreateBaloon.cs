﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateBaloon : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform[] spawnPoints;
    public GameObject[] baloons;
    void Start()
    {
        StartCoroutine(StartSpawning());
    }

    IEnumerator StartSpawning() {
        yield return new WaitForSeconds(4);
        for (int i = 0; i < 3; i++) {
            Instantiate(baloons[i], spawnPoints[i].position, Quaternion.identity);
        }
        StartCoroutine(StartSpawning());
    }
    // Update is called once per frame
    
}
